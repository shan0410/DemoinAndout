package com.fff.demoinandout;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.id)
    EditText id;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.idcard)
    EditText idcard;
    @BindView(R.id.gender)
    EditText gender;
    @BindView(R.id.devId)
    EditText devId;


    private Unbinder mUnbinder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mUnbinder = ButterKnife.bind(this);

    }

    public void updata(View view){
        SysRecord record = new SysRecord();
        record.setId(id.getText().toString());
        record.setName(name.getText().toString());
        record.setIdcardimg(photoBase64("/sdcard/1.jpeg"));
        record.setPersonimg(photoBase64("/sdcard/4.jpg"));
        record.setIdcard(idcard.getText().toString());
        record.setGender(gender.getText().toString());
        //record.set(devId.getText().toString());

        UpdateThread re = new UpdateThread(record);
        re.start();
    }

    public class UpdateThread extends  Thread{
        SysRecord record =null;
        public UpdateThread(SysRecord record)
        {
            this.record = record;
        }
        @Override
        public void  run(){
            String studentsStr = JSON.toJSONString(record);
            OkHttpClient mOkHttpClient = new OkHttpClient();
            FormBody body = new FormBody.Builder()
                    .add("record", studentsStr)
                    .build();
            Request request = new Request.Builder()
                    .url("http://192.168.43.220:8080/android/record")
                    .post(body)
                    .build();
            Call call = mOkHttpClient.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    String responseStr1 ="1";
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String responseStr = response.body().string();
                }


            });

        }

    }

    public static String photoBase64(String photoPath) {
        //读取图片
        String photoBase64 = null;
        //字节流转图片对象
        InputStream inString = null;
        try {
            byte[] data = null;
            inString = new FileInputStream(photoPath);
            data = new byte[inString.available()];
            inString.read(data);
            //对字节数组Base64编码
            photoBase64 =  encodeStr(data);
        } catch (IOException ex) {
             
        } finally {
            try {
                inString.close();
            } catch (IOException ex) {
                 
            }
        }
        return photoBase64;
    }

    public static String encodeStr(byte[] b) {
        b = Base64.encode(b, Base64.DEFAULT);
        String s = new String(b);
        return s;
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }
}