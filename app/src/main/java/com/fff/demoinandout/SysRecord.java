package com.fff.demoinandout;


/**
 * 采集记录表对象 sys_record
 *
 * @author fhs
 * @date 2020-12-11
 */
public class SysRecord
{
    private static final long serialVersionUID = 1L;

    /** 接口版本号 */

    private String ver;

    /** 唯一编号UUID */
    private String id;

    /** 采集时间 */

    private String passtime;

    /** 姓名 */

    private String name;

    /** 性别 */

    private String gender;

    /** 民族 */

    private String nation;

    /** 证件号 */

    private String idcard;

    /** 证件类型 */

    private String cardtype;

    /** 国籍或所在地区代码 */

    private String countryorareacode;

    /** 国籍或所在地区名称 */

    private String countryorareaname;

    /** 证件版本号 */

    private String cardversion;

    /** 当次申请受理机关代码 */

    private String currentapplyorgcode;

    /** 签发次数 */

    private String signnum;

    /** 出生年月日 */

    private String birthday;

    /** 住址信息 */

    private String address;

    /** 签发机关 */

    private String authority;

    /** 有效期开始时间 */

    private String validitystart;

    /** 有效期结束时间 */

    private String validityend;

    /** 身份证照片 */

    private String idcardimg;

    /** 采集照片 */

    private String personimg;

    /** 行政区划代码 */

    private String areacode;

    /** 经度 */

    private String x;

    /** 纬度 */

    private String y;

    /** 设备编号 */

    private String equipmentid;

    /** 设备名称 */

    private String equipmentname;

    /** 设备类型 */

    private String equipmenttype;

    /** 警务站编号 */

    private String stationid;

    /** 警务站名称 */

    private String stationname;

    /** 回传的接口URL */

    private String backurl;

    /** 设备所在位置名称 */

    private String location;

    /** 出入标识 */

    private String status;

    /** 小区名称 */

    private String dareaname;

    /** 小区编号 */

    private String dareacode;

    /** 场所类别 */

    private String placetype;

    /** 人员类别 */

    private String identity;

    /** 实际家庭住址 */

    private String homeplace;

    /** 联系电话 */

    private String contact;

    /** 人证是否一致 */

    private String isconsist;

    /** 人证相似度 */

    private String comparescore;

    /** 开门方式 */

    private String openmode;

    /** 访问事由 */

    private String visitreason;

    /**  */

    private String visitor;

    /** 温度 */

    private String temperature;

    /** 部门id */

    private Integer deptId;

    public void setVer(String ver)
    {
        this.ver = ver;
    }

    public String getVer()
    {
        return ver;
    }
    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setPasstime(String passtime)
    {
        this.passtime = passtime;
    }

    public String getPasstime()
    {
        return passtime;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }
    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public String getGender()
    {
        return gender;
    }
    public void setNation(String nation)
    {
        this.nation = nation;
    }

    public String getNation()
    {
        return nation;
    }
    public void setIdcard(String idcard)
    {
        this.idcard = idcard;
    }

    public String getIdcard()
    {
        return idcard;
    }
    public void setCardtype(String cardtype)
    {
        this.cardtype = cardtype;
    }

    public String getCardtype()
    {
        return cardtype;
    }
    public void setCountryorareacode(String countryorareacode)
    {
        this.countryorareacode = countryorareacode;
    }

    public String getCountryorareacode()
    {
        return countryorareacode;
    }
    public void setCountryorareaname(String countryorareaname)
    {
        this.countryorareaname = countryorareaname;
    }

    public String getCountryorareaname()
    {
        return countryorareaname;
    }
    public void setCardversion(String cardversion)
    {
        this.cardversion = cardversion;
    }

    public String getCardversion()
    {
        return cardversion;
    }
    public void setCurrentapplyorgcode(String currentapplyorgcode)
    {
        this.currentapplyorgcode = currentapplyorgcode;
    }

    public String getCurrentapplyorgcode()
    {
        return currentapplyorgcode;
    }
    public void setSignnum(String signnum)
    {
        this.signnum = signnum;
    }

    public String getSignnum()
    {
        return signnum;
    }
    public void setBirthday(String birthday)
    {
        this.birthday = birthday;
    }

    public String getBirthday()
    {
        return birthday;
    }
    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getAddress()
    {
        return address;
    }
    public void setAuthority(String authority)
    {
        this.authority = authority;
    }

    public String getAuthority()
    {
        return authority;
    }
    public void setValiditystart(String validitystart)
    {
        this.validitystart = validitystart;
    }

    public String getValiditystart()
    {
        return validitystart;
    }
    public void setValidityend(String validityend)
    {
        this.validityend = validityend;
    }

    public String getValidityend()
    {
        return validityend;
    }
    public void setIdcardimg(String idcardimg)
    {
        this.idcardimg = idcardimg;
    }

    public String getIdcardimg()
    {
        return idcardimg;
    }
    public void setPersonimg(String personimg)
    {
        this.personimg = personimg;
    }

    public String getPersonimg()
    {
        return personimg;
    }
    public void setAreacode(String areacode)
    {
        this.areacode = areacode;
    }

    public String getAreacode()
    {
        return areacode;
    }
    public void setX(String x)
    {
        this.x = x;
    }

    public String getX()
    {
        return x;
    }
    public void setY(String y)
    {
        this.y = y;
    }

    public String getY()
    {
        return y;
    }
    public void setEquipmentid(String equipmentid)
    {
        this.equipmentid = equipmentid;
    }

    public String getEquipmentid()
    {
        return equipmentid;
    }
    public void setEquipmentname(String equipmentname)
    {
        this.equipmentname = equipmentname;
    }

    public String getEquipmentname()
    {
        return equipmentname;
    }
    public void setEquipmenttype(String equipmenttype)
    {
        this.equipmenttype = equipmenttype;
    }

    public String getEquipmenttype()
    {
        return equipmenttype;
    }
    public void setStationid(String stationid)
    {
        this.stationid = stationid;
    }

    public String getStationid()
    {
        return stationid;
    }
    public void setStationname(String stationname)
    {
        this.stationname = stationname;
    }

    public String getStationname()
    {
        return stationname;
    }
    public void setBackurl(String backurl)
    {
        this.backurl = backurl;
    }

    public String getBackurl()
    {
        return backurl;
    }
    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getLocation()
    {
        return location;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDareaname(String dareaname)
    {
        this.dareaname = dareaname;
    }

    public String getDareaname()
    {
        return dareaname;
    }
    public void setDareacode(String dareacode)
    {
        this.dareacode = dareacode;
    }

    public String getDareacode()
    {
        return dareacode;
    }
    public void setPlacetype(String placetype)
    {
        this.placetype = placetype;
    }

    public String getPlacetype()
    {
        return placetype;
    }
    public void setIdentity(String identity)
    {
        this.identity = identity;
    }

    public String getIdentity()
    {
        return identity;
    }
    public void setHomeplace(String homeplace)
    {
        this.homeplace = homeplace;
    }

    public String getHomeplace()
    {
        return homeplace;
    }
    public void setContact(String contact)
    {
        this.contact = contact;
    }

    public String getContact()
    {
        return contact;
    }
    public void setIsconsist(String isconsist)
    {
        this.isconsist = isconsist;
    }

    public String getIsconsist()
    {
        return isconsist;
    }
    public void setComparescore(String comparescore)
    {
        this.comparescore = comparescore;
    }

    public String getComparescore()
    {
        return comparescore;
    }
    public void setOpenmode(String openmode)
    {
        this.openmode = openmode;
    }

    public String getOpenmode()
    {
        return openmode;
    }
    public void setVisitreason(String visitreason)
    {
        this.visitreason = visitreason;
    }

    public String getVisitreason()
    {
        return visitreason;
    }
    public void setVisitor(String visitor)
    {
        this.visitor = visitor;
    }

    public String getVisitor()
    {
        return visitor;
    }
    public void setTemperature(String temperature)
    {
        this.temperature = temperature;
    }

    public String getTemperature()
    {
        return temperature;
    }
    public void setDeptId(Integer deptId)
    {
        this.deptId = deptId;
    }

    public Integer getDeptId()
    {
        return deptId;
    }


}
